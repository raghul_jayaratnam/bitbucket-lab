BitBucket URL Assignment
Dr. John Noll
Tuesday, 2 Nov, 2021

Project Git Repository

1.  Create a Bitbucket repository for your group. Name your repository
    “A_group-XX” or “B_group-XX”, where “XX” is replaced by your group
    number.

    Please follow this naming convention so we know which group owns a
    repository.

2.  Invite j.noll@herts.ac.uk to become a member of your repository’s
    team.

    1.  Select “Repository settings” from the left menu, then “User and
        group access.”

    2.  Enter j.noll@herts.ac.uk in the “Users” dialog.

    3.  select “Write” or “Admin” from the dropdown (it’s necessary to
        grant write access so we can give you feedback on your
        coursework by committing files to your repository).

        [User access]

    4.  Click the “Add” button.

    Caution: Make sure your repository is accessible to
    j.noll@herts.ac.uk. Since we will be using Bitbucket for coursework
    submissions after this week, if you don’t make it accessible, or
    don’t follow the naming convention, you won’t receive credit for
    your submissions. You will need a BitBucket Educational License to
    have more than five members of your repository.

3.  Copy the URL for your repository from BitBucket (or, from
    .git/config).

4.  Paste the URL for your repository into the Canvas assignment
    “Website URL” box.

    [Submit URL]

5.  Submit the URL for your repository the deadline (23:59 2021-11-05).

6.  Have another member of your group verify that you have submitted the
    correct URL, by copying the URL from Canvas, then cloning a new
    workspace using git clone.

How to find your Bitbucket URL

1.  Visit your Bitbucket.org page.

2.  Select your repository.

    [Select Repository]

3.  Select “Clone” in the upper right corner.

    [Bitbucket Repository]

4.  Copy the URL only (not the “git clone” part!).

    [Bitbucket Clone]
