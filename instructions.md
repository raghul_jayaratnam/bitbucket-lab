<!--
If you see this comment, you are reading the wrong file!
View #filename(instructions.pdf) or #(instructions.html)
per instructions in #filename(README.txt).
-->

---
author: #module_leader
date: #date
documentclass: hitec
fontfamily: opensans
fontsize: 12
...

## Project Git Repository

#. Create a Bitbucket repository for your group.  Name your repository
"A_group-XX" or "B_group-XX", where "XX" is replaced by your group number.

    Please follow this naming convention so we know which group owns a repository. 


#. Invite `j.noll@herts.ac.uk` to become a member of your repository's
 team.

    #. Select "Repository settings" from the left menu, then "User and
     group access."

    #. Enter `j.noll@herts.ac.uk` in the "Users" dialog.

    #. select "Write" or "Admin" from the dropdown (it's necessary to grant write
access so we can give you feedback on your coursework by committing
files to your repository).

        ![User access](bitbucket-user-access.png){height=70%}\


    #. Click the "Add" button.

    _Caution_: Make sure your repository is accessible to
j.noll@herts.ac.uk. Since we will be using Bitbucket for coursework
submissions after this week, if you don't make it accessible, or don't
follow the naming convention, you won't receive credit for your
submissions.  You will need a BitBucket Educational License to have
more than five members of your repository.

#. Copy the URL for your repository from BitBucket (or, from
 #filename(.git/config)).
 
#. Paste the  URL for your repository into the Canvas assignment
 "Website URL" box.

    ![Submit URL](canvas-submit-url.png)\

#. Submit the URL for your repository the deadline (23:59
 #cw_git_due).
 
#. Have another member of your group verify that you have submitted
 the correct URL, by copying the URL from Canvas, then cloning a *new*
 workspace using #command(git clone).

### How to find your Bitbucket URL

#. Visit your Bitbucket.org page.

#. Select your repository.

    ![Select Repository](bitbucket-home.png){width=70%}\


#. Select "Clone" in the upper right corner.

    ![Bitbucket Repository](bitbucket-repo.png){width=70%}\


#. Copy the URL _only_ (**not the "git clone" part!**).

    ![Bitbucket Clone](bitbucket-clone.png){width=70%}\
